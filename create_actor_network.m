function actor = create_actor_network(inputsize,Actinputsize)
actorNetwork = [featureInputLayer(inputsize) 
    fullyConnectedLayer(32)
    reluLayer
    fullyConnectedLayer(16)
    reluLayer
    fullyConnectedLayer(8)
    fullyConnectedLayer(Actinputsize)];
actor = dlnetwork(actorNetwork);
%actorNetwork = [actor.Layers(1:5, 1)
%layersTransfer
%actor.Layers(6:end, 1)];
%actor = dlnetwork(actorNetwork);
end