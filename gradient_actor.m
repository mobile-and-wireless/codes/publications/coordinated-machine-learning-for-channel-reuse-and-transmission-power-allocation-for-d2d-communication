function gradients = gradient_actor(network,critic, input_data,loss)
    gradients = dlfeval(@computeactor_gradients, network,critic, input_data,loss);
end