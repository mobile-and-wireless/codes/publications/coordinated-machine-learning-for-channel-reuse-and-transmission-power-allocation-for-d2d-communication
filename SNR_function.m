function SNR = SNR_function(Number_SN,Number_RN,PL,Pt_UE, Noise_var)
SNR=zeros(Number_SN,Number_RN);
for i=1:Number_SN
    for j=1:Number_RN
       SNR(i,j) = Pt_UE-PL(i,j)-Noise_var;
    end
end
end