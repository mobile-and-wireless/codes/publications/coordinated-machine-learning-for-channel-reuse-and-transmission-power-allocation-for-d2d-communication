function gradients = compute_gradients(network, input_data1, input_data2,dist_loss,target_Q_values) 
    predictions = predict(network, input_data1, input_data2);
    target_Q_values = reshape(target_Q_values,length(predictions),[]);
    loss = crossentropy(dlarray((mean(target_Q_values,2)),'BC'),predictions)+sum(dist_loss);
    gradients = dlgradient(real(loss), network.Learnables);
end