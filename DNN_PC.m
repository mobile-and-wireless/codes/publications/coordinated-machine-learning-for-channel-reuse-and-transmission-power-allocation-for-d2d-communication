function [layers,options] = DNN_PC(num_d2d_pairs,MaxEpochs)
layers = [sequenceInputLayer(num_d2d_pairs)
          fullyConnectedLayer(num_d2d_pairs)
          sigmoidLayer
          fullyConnectedLayer(60)
          sigmoidLayer
          fullyConnectedLayer(30)
          sigmoidLayer
          fullyConnectedLayer(20)
          sigmoidLayer
          fullyConnectedLayer(num_d2d_pairs)
          regressionLayer
];
options = trainingOptions('adam', ...
    'MaxEpochs', MaxEpochs, ...
    'MiniBatchSize', 32, ...
    'InitialLearnRate', 0.01);
end