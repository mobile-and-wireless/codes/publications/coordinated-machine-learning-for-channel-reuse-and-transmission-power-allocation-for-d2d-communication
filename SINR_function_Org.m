function [SINR,interference,intNO,gain] = SINR_function_Org(Number_MS,RSS, PL, N_W,BS_assoc,Pt,Bn,Channel_reuse)
SINR=zeros(Number_MS,1);
NI=zeros(Number_MS,1);
interference = zeros(1,Number_MS);
for i=1:Number_MS
    for j=1:Number_MS
        if BS_assoc(i,2)~=j
            NI(i,1) = NI(i,1) + 10.^-3*10.^( RSS(i,j)/10);
            interference(j,i) = Channel_reuse(i,1).*(10*log10(NI(i,1)));
        end
    end
    intNO(i) = Channel_reuse(i,1)*(10*log10(NI(i,1)/0.001));
    NI(i,1) = Channel_reuse(i,1)*NI(i,1) + N_W;   
    NI(i,1)=10*log10(NI(i,1)/0.001);
    SINR(i,1)=Pt(i)-PL(i,BS_assoc(i,2))-NI(i,1);
end
gain = real(Bn.*log2(1+SINR.'));
end