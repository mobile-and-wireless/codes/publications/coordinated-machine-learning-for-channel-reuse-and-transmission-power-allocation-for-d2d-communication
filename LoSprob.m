function sigma = LoSprob(Number_SN,Number_RN,d)
sigma=zeros(Number_SN,Number_RN);
for i=1:Number_SN
    for j=1:Number_RN
       sigma(i,j) = min(18/d(i,j),1)*(1-exp(-d(i,j)/63))+exp(-d(i,j)/63);
    end
end
end