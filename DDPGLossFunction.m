function combinedLoss = DDPGLossFunction(tot,cmin,threshold,IntNO1,P_min,P_max,predicted_PC)
minpower_penalty = max(0, P_min - predicted_PC);
maxpower_penalty = max(0, predicted_PC-P_max);
loss_DDPG = -log(sum(tot)) + max(0,cmin-tot) + max(0,IntNO1-threshold);
combinedLoss = sum(loss_DDPG);
end