function RSS = RSS_function(Number_MS,Number_BS,Pt,PL,sigma,Attenuation_obstacle)
RSS=zeros(Number_MS,Number_BS);
for i=1:Number_MS
    for j=1:Number_BS
        if rand<sigma(i,j)
            RSS(i,j) = Pt-PL(i,j);
        else
            RSS(i,j) = Pt-PL(i,j)-Attenuation_obstacle;
        end
    end
end
end