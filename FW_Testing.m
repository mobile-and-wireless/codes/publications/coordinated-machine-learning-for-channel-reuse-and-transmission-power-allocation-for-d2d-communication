clc
close all
clear all
Number_BS = 4;
f = 2;
Pt = 23;
MaxEpochs = 1000;
BW = 20*10.^6;
Attenuation_obstacle = 10;
d2d_max = 50;
threshold = -20;
NoOfFBSteps = 100;
PosBS = [250 250 30; 250 750 30; 750 250 30; 750 750 30];
load('D2D_positions.mat')
load('B2MS_positions.mat')
PosD2D = PosD2Dinit;
N_W = (BW*4*10.^-12)/10.^9;
N_dBm = 10*log10(N_W/0.001);
Number_MS=16;
Bn_UE = (BW/(8))/1e6;
cmin = 2;
Pmin = 0;
gamma = 0.99;
actor_lr = 0.001;
critic_lr = 0.01;
load(['Trained DNN-PC' num2str(Number_MS/2) 'D2DUEs.mat']);
load(['Testing Dataset' num2str(Number_MS/2) 'D2DUEs.mat']);
Channel_reuse = zeros(Number_MS/2,1);
%Channel_reuseWOF = zeros(Number_MS/2,NoOfFBSteps);
%rewardWOF = zeros(Number_MS/2,Number_MS/2,NoOfFBSteps);
%reward12WOF = zeros(Number_MS/2,Number_MS/2,NoOfFBSteps);
%reward = zeros(Number_MS/2,Number_MS/2,NoOfFBSteps);
%tot_capacityWOF = zeros(Number_MS/2,NoOfFBSteps);
%NORU_NODNNCQDNNPC_tot_capacity = zeros(1,Number_MS/2);
%tot_capacity_WOCQ_PC = zeros(Number_MS/2,NoOfFBSteps);
%NORU_NODNNCQDNNPC_tot_capacityWOF = zeros(Number_MS/2,NoOfFBSteps);
%tot_capacity_WOCQ_PCWOF = zeros(Number_MS/2,NoOfFBSteps);
Bn = zeros(1,Number_MS/2)+Bn_UE;   
d = distance(Number_MS,Number_BS,PosMS,PosBS,1);
d2d_dist  = distance_D2D(Number_MS/2,PosD2D,d2d_max);
PL = pathloss(Number_MS,Number_BS,d,f);
PL_D2D = pathloss(Number_MS/2,Number_MS/2,d2d_dist,f);
sigma = LoSprob(Number_MS,Number_BS,d);
sigma_D2D = LoSprob(Number_MS/2,Number_MS/2,d2d_dist);
RSS = RSS_function(Number_MS,Number_BS,Pt,PL,sigma,Attenuation_obstacle);
RSS_D2D = RSS_function(Number_MS/2,Number_MS/2,Pt,PL_D2D,sigma_D2D,Attenuation_obstacle);
SNR = SNR_function(Number_MS,Number_BS,PL,Pt,N_dBm);
SNR_D2D = SNR_function(Number_MS/2,Number_MS/2,PL_D2D,Pt,N_dBm);
BS_assoc = BS_association(Number_MS,RSS);
BS_assoc_D2D = BS_association(Number_MS/2,RSS_D2D);
inputsize = Number_MS;
Actinputsize = Number_MS/2;
actornet = create_actor_network(inputsize,Actinputsize);
criticnet = create_critic_network(inputsize,Actinputsize);
obsInfo = rlNumericSpec([inputsize 1]);
actInfo = rlNumericSpec([Actinputsize 1],'LowerLimit', 0, 'UpperLimit', 1); 
actor = rlContinuousDeterministicActor(actornet,obsInfo,actInfo);
critic = rlQValueFunction(criticnet,obsInfo,actInfo,...
ObservationInputNames="netOin", ...
ActionInputNames="netAin");
replay_buffer = [];
%netPCWOF = netPC;
test_input_PC = test_data;
for SimStep=1:NoOfFBSteps    
    predicted_PC= predict(netPC, test_input_PC.');
    [SINR,Interference,IntNO,gain] = SINR_function_Org(Number_MS/2,RSS_D2D, PL_D2D, N_W,BS_assoc_D2D,predicted_PC,Bn,Channel_reuse); 
    l_pc = -log(gain) + max(0,cmin-gain) + max(0,Pt-sum(Channel_reuse.*predicted_PC).') + max(0,sum(Channel_reuse.*predicted_PC).'-Pmin);
    state = [BS_assoc_D2D(:,1);predicted_PC]; 
    state = state.';
    action = select_action(actornet, state);
    [next_state, reward] = perform_action(state, action, BS_assoc_D2D(:,1), predicted_PC, Bn.', cmin, Interference,N_dBm);
    experience = struct('state', state, 'action', action, 'reward', reward, 'next_state', next_state);
    replay_buffer = [replay_buffer, experience];
    criticnet = update_critic(actornet, criticnet, replay_buffer, critic_lr, gamma, l_pc);
    actornet = update_actor(actornet, criticnet, replay_buffer, actor_lr, l_pc);
    state = next_state;
    Channel_reuse = action;
    Channel_reuse = extractdata(Channel_reuse);
    l_cr = ((Channel_reuse.').*(Bn.*log2(1+10.^(SINR.'/10)))) + ((Bn.*log2(1+10.^(SINR.'/10))));
    gradientsCR_PC = dlfeval(@DDPGLossFunction, l_cr,cmin,threshold,IntNO.',Pmin,Pt,predicted_PC);
    params = [];
    averageGrad = [];
    averageSqGrad = [];
    if (sum(Channel_reuse.*predicted_PC)<Pt)
        break;
    end
    for i = 1:numel(netPC.Layers)
     if isa(netPC.Layers(i), 'nnet.cnn.layer.FullyConnectedLayer') || isa(netPC.Layers(i), 'nnet.cnn.layer.Convolution2DLayer')
        params = [params; netPC.Layers(i).Weights(:); netPC.Layers(i).Bias(:)];
     end
    end
    updatedParams = adamupdate(params, gradientsCR_PC,averageGrad,averageSqGrad,MaxEpochs);
    k = 1;
    modify_able_NN = netPC.saveobj;
    for i = 1:numel(modify_able_NN.Layers)
     if isa(modify_able_NN.Layers(i), 'nnet.cnn.layer.FullyConnectedLayer') || isa(modify_able_NN.Layers(i), 'nnet.cnn.layer.Convolution2DLayer')
        numWeights = numel(modify_able_NN.Layers(i).Weights);
        numBias = numel(modify_able_NN.Layers(i).Bias);
        modify_able_NN.Layers(i).Weights = reshape(updatedParams(k:k+numWeights-1), size(modify_able_NN.Layers(i).Weights));
        k = k + numWeights;
        modify_able_NN.Layers(i).Bias = reshape(updatedParams(k:k+numBias-1), size(modify_able_NN.Layers(i).Bias));
        k = k + numBias;
     end
    end
    netPC = netPC.loadobj(modify_able_NN);
end