function [userPowers] = DNNPC_output(bandwidth,channelGain,noise_var)
targetCap = 2; 
initialPower = 23;
targetCap = 10^(targetCap/10);
noisePower = 10^(noise_var/10);
userPower = rand(1, length(channelGain));
currentPower = userPower.*abs(channelGain);
interferencePower = sum(currentPower) - currentPower;
actualSNR = currentPower ./ (interferencePower + noisePower);
Cap_measured = bandwidth*log2(1 + actualSNR)/1e6;
userPowers = initialPower * (targetCap ./ abs(10*log10(Cap_measured)));
userPowers = 10*log10(userPowers);
end