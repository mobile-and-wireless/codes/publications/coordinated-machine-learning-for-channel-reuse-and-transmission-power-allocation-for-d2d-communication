function action = select_action(actor, state)
    raw_action = predict(actor,dlarray(state,'BC'));
    action_probs = sigmoid(raw_action);
    threshold = 0.5;
    action = action_probs < threshold;
end