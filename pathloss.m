function PL = pathloss(Number_SN,Number_RN,dist,fc)
PL=zeros(Number_SN,Number_RN);
for i=1:Number_SN
    for j=1:Number_RN
       PL(i,j) = 35.2+35*log10(dist(i,j))+26*log10(fc/2);
    end
end
end