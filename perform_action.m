function [next_state, reward] = perform_action(state, ~, predicted_CQ, predicted_PC, Bn, ~, Interference,N_dBm)
    next_state = state;
    cap = Bn.*log2(1+((predicted_CQ.*predicted_PC)./(N_dBm+Interference)));
    reward = cap;
end