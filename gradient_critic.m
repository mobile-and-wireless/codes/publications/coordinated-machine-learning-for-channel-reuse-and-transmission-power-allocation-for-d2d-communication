function gradients = gradient_critic(network, input_data1,input_data2,loss,target_Q_values)
    gradients = dlfeval(@compute_gradients, network, input_data1, input_data2,loss,target_Q_values);
end