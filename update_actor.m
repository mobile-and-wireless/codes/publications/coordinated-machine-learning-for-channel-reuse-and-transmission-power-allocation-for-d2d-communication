function actor = update_actor(actor, critic, batch, actor_lr, loss)
    states = cat(1, batch.state);
    actor_gradients = gradient_actor(actor,critic, states,loss);
    actor = adamupdate(actor, actor_gradients,[],[],1000,actor_lr);
    %actor.Learnables = actor_gradients;
end