function BS_assoc = BS_association(Number_SN,SNR)
BS_assoc=zeros(Number_SN,2);
for i=1:Number_SN
    [BS_assoc(i,1),BS_assoc(i,2)]=max(SNR(i,:));
end
end