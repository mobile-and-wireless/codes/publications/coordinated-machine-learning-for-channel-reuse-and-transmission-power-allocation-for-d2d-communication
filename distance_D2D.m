function d = distance_D2D(Number_SN,PosD2D,d2d_max)
d=zeros(Number_SN,Number_SN);
for i=1:Number_SN
    for j=1:Number_SN
        d(i,j)=sqrt((abs(PosD2D(1,2*j-1)-PosD2D(1,2*i+1)))^2 + (abs(PosD2D(1,j+1)-PosD2D(1,2*i+2)))^2);
    end
end
d(d>d2d_max) = d2d_max;
end