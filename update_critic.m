function critic = update_critic(actornet, critic, batch, critic_lr, gamma,fed_loss)
    states = cat(1, batch.state);
    rewards = cat(1, batch.reward);
    next_states = cat(1, batch.next_state);
    action = select_action(actornet, next_states);
    target_next_Q_values = predict(critic, dlarray(next_states,'BC'),action);
    rewards = reshape(rewards, [],size(target_next_Q_values,2));
    target_Q_values = rewards + gamma .* target_next_Q_values;
    target_Q_values = reshape(target_Q_values, 1,[]);
    averageGrad = [];
    averageSqGrad = [];
    critic_gradients = gradient_critic(critic, dlarray(states,'BC'), action,fed_loss,target_Q_values);
    critic = adamupdate(critic, critic_gradients,averageGrad,averageSqGrad,1000,critic_lr);
end
