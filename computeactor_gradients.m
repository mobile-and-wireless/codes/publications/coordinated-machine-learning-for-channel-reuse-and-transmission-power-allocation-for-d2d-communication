function gradients = computeactor_gradients(network,critic, input_data,loss)
    pred = predict(network, dlarray(input_data,'BC'));
    grad = predict(critic, dlarray(input_data,'BC'),pred);
    scalarValue = grad+sum(loss);
    gradients = dlgradient(dlarray(real(scalarValue(end)),'BC'), network.Learnables);
end