clc
close all
clear all
Area_x=1000;
Area_y=1000;
Number_MS=96;
Number_BS=4;
MaxEpochs=1000;
f=2;
Pt=23;
BW = 20*10.^6;
Attenuation_obstacle=10;
d2d_max = 50;
PosBS = [250 250 30; 250 750 30; 750 250 30; 750 750 30];
load('D2D_positions.mat')
load('B2MS_positions.mat')
PosD2D = PosD2Dinit;
N_W = (BW*4*10.^-12)/10.^9;
N_dBm = 10*log10(N_W/0.001);       
d = distance(Number_MS,Number_BS,PosMS,PosBS,1);
d2d_dist  = distance_D2D(Number_MS/2,PosD2D,d2d_max); 
PL = pathloss(Number_MS,Number_BS,d,f);
PL_D2D = pathloss(Number_MS/2,Number_MS/2,d2d_dist,f);
sigma = LoSprob(Number_MS,Number_BS,d);
sigma_D2D = LoSprob(Number_MS/2,Number_MS/2,d2d_dist);
RSS = RSS_function(Number_MS,Number_BS,Pt,PL,sigma,Attenuation_obstacle);
RSS_D2D = RSS_function(Number_MS/2,Number_MS/2,Pt,PL_D2D,sigma_D2D,Attenuation_obstacle);
SNR = SNR_function(Number_MS,Number_BS,PL,Pt,N_dBm);
SNR_D2D = SNR_function(Number_MS/2,Number_MS/2,PL_D2D,Pt,N_dBm);
BS_assoc = BS_association(Number_MS,RSS);
BS_assoc_D2D = BS_association(Number_MS/2,RSS_D2D);
[layersPC,optionsPC] = DNN_PC(Number_MS/2,MaxEpochs);
[DNNPC_Targets] = DNNPC_output(BW,BS_assoc_D2D(:,1).',N_dBm);
Features_DNNPC = BS_assoc_D2D(:,1);
[netPC, infoPC] = trainNetwork(Features_DNNPC, DNNPC_Targets.', layersPC, optionsPC);     
save(['Trained DNN-PC' num2str(Number_MS/2) 'D2DUEs.mat'],'netPC');